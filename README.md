## Internationalization of default site language

After enabling this module you will find "**Default language**" textarea right

below "**Original text**". After you will set some value string will be

translated to new value using english language.
