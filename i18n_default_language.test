<?php

/**
 * @file
 * Tests for i18n_default_language.module.
 */

/**
 * Functional test for string translation and validation.
 */
class I18nDefaultLanguageFunctionalTest extends DrupalWebTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Translation of source language',
      'description' => 'Adds a new locale and translates its name. Checks translation to new language and to default language.',
      'group' => 'Locale',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp('locale', 'i18n_default_language');
  }

  /**
   * Tests the validation of the translation input.
   */
  public function testStringValidation() {
    // User to add language and strings.
    $admin_user = $this->drupalCreateUser(array(
      'administer languages',
      'access administration pages',
      'translate interface',
    ));
    $this->drupalLogin($admin_user);
    $langcode = 'xx';
    // The English name for the language. This will be translated.
    $name = $this->randomName(16);
    // The native name for the language.
    $native = $this->randomName(16);
    // The domain prefix.
    $prefix = $langcode;

    // Add custom language.
    $edit = array(
      'langcode' => $langcode,
      'name' => $name,
      'native' => $native,
      'prefix' => $prefix,
      'direction' => '0',
    );
    $this->drupalPost('admin/config/regional/language/add', $edit, t('Add custom language'));
    // @codingStandardsIgnoreStart
    t($name, array(), array('langcode' => $langcode));
    // @codingStandardsIgnoreEnd
    // Reset locale cache.
    $search = array(
      'string' => $name,
      'language' => 'all',
      'translation' => 'all',
      'group' => 'all',
    );
    $this->drupalPost('admin/config/regional/translate/translate', $search, t('Filter'));
    // Find the edit path.
    $content = $this->drupalGetContent();
    $this->assertTrue(preg_match('@(admin/config/regional/translate/edit/[0-9]+)@', $content, $matches), 'Found the edit path.');
    $path = $matches[0];
    $edit = array(
      "translations[$langcode]" => $name . $langcode,
      'default_language_translations' => $name . 'default',
    );
    $this->drupalPost($path, $edit, t('Save translations'));
    $this->drupalGet($path);
    $this->assertText($name . $langcode);
    $this->assertText($name . 'default');
  }

}
